import time
from Browser import Browser
import FileLocations

brwsr = Browser()

timeout = 5
time_started = time.time()

# url building tools
url_base = 'https://du.primo.exlibrisgroup.com/discovery/'
url_search = 'search?'
url_display = 'fulldisplay?'
url_ext = 'tab=Everything&vid=01UODE_INST:01UODE_MAIN&lang=en&search_scope=MyInst_and_CI'
url_ext_query_base = '&query=any,contains,'


# using more search terms is always more restrictive
# can use capital OR to combine results
f = open(FileLocations.fileLocationQueriesInput, "r")
url_ext_query_words = [x.removesuffix("\n") for x in f.readlines()]
f.close()

f = open(FileLocations.fileLocationQueriesVisited, "r")
searched_already = [x.removesuffix("\n") for x in f.readlines()]
f.close()


f = open(FileLocations.fileLocationArticlesOutput, "r")
documents_already = [x.removesuffix("\n") for x in f.readlines()]
f.close()

documents = {}
starting_count = 0
if len(documents_already) != 0:
    for d in documents_already:
        if d != "":
            doc_pieces = str.split(d, ",")
            documents[doc_pieces[0]] = doc_pieces[1]
    starting_count = len(documents)
    print(f"Preloaded {starting_count} existing documents")

count_duplicates = 0

for search in url_ext_query_words:
    if search in searched_already:
        print(f"Already searched for '{search}', will skip. Remove line containing '{search}' from {FileLocations.fileLocationQueriesVisited} if not correct.")
        continue
    else:
        print(f"Searching for '{search}'")

    search_term = search.replace(" ", "%20")
    index_page = 0

    pages_left = True
    while pages_left:
        url_ext_offset = '&offset=' + str(int(index_page))
        url = url_base + url_search + url_ext + url_ext_query_base + search_term + url_ext_offset
        # print(url)

        strSearch = 'docid='
        strSearchEnd = '&'   
        brwsr.GoTo(url)

        # this was found by investigating the page for the right element id, should you need to change it
        strSoup = brwsr.WaitFor('searchResultsContainer', strSearch, 90)


        try:
            index_href = strSoup.index(strSearch)
        except:
            index_href = -1
        if index_href != -1:
            while index_href != -1:
                index_href_end = strSoup.index(strSearchEnd, index_href + len(strSearch))
                docid = strSoup[index_href + len(strSearch): index_href_end]
                if docid not in documents:
                    # TODO why is this the way it is
                    documents[docid] = f"https://du.primo.exlibrisgroup.com/discovery/fulldisplay?docid={docid}&context=PC&vid=01UODE_INST:01UODE_MAIN&lang=en&search_scope=MyInst_and_CI&adaptor=Primo%20Central&tab=Everything&query=any,contains,hci%20social%20survey%20paper&offset=0"
                    # documents[docid] = url_base + url_display + f"docid={docid}" + url_ext + url_ext_query_base + search_term + url_ext_offset

                try:
                    index_href = strSoup.index(strSearch, index_href_end)
                except:
                    index_href = -1
        else:
            print(f"Finished searching '{search}'.")
            pages_left = False #nothing was found on that page after timeout

        # we get 10 links at a time
        # why not more? thats a javascript button, doesn't go to the url
        # therfor, not worth it since it already does the job for the most part
        index_page += 10

        f = open(FileLocations.fileLocationArticlesOutput, "w")
        for k in documents:
            f.write(f"{k},{documents[k]}\n")
        f.close()

        print(f"Recording {len(documents) - starting_count} unique articles over {(time.time() - time_started) / 60:0.2f} minutes.")
    
    f = open(FileLocations.fileLocationQueriesVisited, "a")
    f.write(f"{search}\n")
    f.close()

Welcome to the Literature Review Automation tool!

This tool will help you gather all relevant articles. 
Enter your desired search terms into search_queries_input.txt, run overnight, and refine your results to provide exactly the background your research needs

Step 00: Setup
Setup.ipynb will automatically install all relevant pip packages

Step 01: Scrape
Scrape.py will find all links related to search terms and store the resulting id and link.

Step 02: Extract
Extract.py will take all links found by extract and get the page specific data related to the scraped link.
NOTE: YOU MUST LOG IN WHEN THE WINDOW FIRST COMES UP TO GAIN FULL ACCESS TO ARTICLE DATA

Step 03: Clean
Clean.py will try to fill in the important gaps in the data, such as year and article type.

Step 04: Display
Display.ipynb provides different ways of viewing the data.

Step ??: Purge
Purge.py will filter out unwanted results from the whole set to those actually relevant to our research.
TODO: Create Purge.py

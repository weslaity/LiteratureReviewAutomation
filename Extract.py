from bs4 import BeautifulSoup
from selenium import webdriver  
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time

import pandas as pd
import numpy as np
from Browser import Browser
import FileLocations

brwsr = Browser()

timeout = 5
time_started = time.time()

f = open(FileLocations.fileLocationArticlesOutput, "r")
entries = f.readlines()
f.close()

url_base = 'https://du.primo.exlibrisgroup.com/discovery/'
url_search = 'search?'
url_display = 'fulldisplay?'
url_ext = 'tab=Everything&vid=01UODE_INST:01UODE_MAIN&lang=en&search_scope=MyInst_and_CI'
url_ext_query_base = '&query=any,contains,'

output_columns = ['ID', 'Title', 'Subjects', 'Description', 'Contents', 'Notes', 'Creation Date', 'Brief']
df = pd.read_csv(FileLocations.fileLocationArticlesData)
index_entry = 0

f = open(FileLocations.fileLocationArticlesBlacklist, "r")
blacklist = [x.removesuffix("\n") for x in f.readlines()]
f.close()

brwsr.GoTo(entries[0].split(",")[1])
time.sleep(30)
print(brwsr.GetCookies())

for e in entries:
    url = e.split(",")[1]
    id = e.split(",")[0]
    if id in df['ID'].values or id in blacklist:
        print(f"Already recorded data for {id}")
        continue

    brwsr.GoTo(url)
    strSoup = brwsr.WaitFor('full-view-container', '', 15)
    if strSoup is None:
        print(f"Skipping {id}")
        
        blacklist.append(id)
        f = open(FileLocations.fileLocationArticlesBlacklist, "w")
        for b in blacklist:
            f.write(f"{b}\n")
        f.close()
        
        continue
    soup = BeautifulSoup(strSoup, 'html.parser')
    # soup = get_html(url, 'full-view-container', '', 90)

    if soup is None:
        continue

    soupTop = soup.find('div', id = "brief")
    soupDetails = soup.find('div', id = "details")

    spansTop = soupTop.select("span")
    soupDetails = soupDetails.select("span")
    irrelevant_span_texts = ['more', 'hide', '', 'Show All', 'Show Less']
    relevant_span_top_texts = [x.text.strip() for x in spansTop if x.text not in irrelevant_span_texts]
    relevant_span_details_texts = [x.text.strip() for x in soupDetails if x.text not in irrelevant_span_texts]
    print([x for x in relevant_span_top_texts ])

    doc_info = {}
    columns = ['Title', 'Additional Title', 'Author', 'Subjects', 'Is Part Of', 'Description', 'Contents', 'Contributor', 'Publisher', 'Creation Date', 'Format', 'Language', 'Notes', 'ISBN/ISSN']
    for i in range(len(columns)):
        c = columns[i]
        # print(c, relevant_span_texts)
        if len(relevant_span_details_texts) != 0 and relevant_span_details_texts[0] == c:
            
            columns_left = [x for x in relevant_span_details_texts if x in columns[i+1:]]
            # print(columns_left)
            try:
                index_next_column = relevant_span_details_texts.index(columns_left[0])
            except:
                index_next_column = 0

            col_info = relevant_span_details_texts[1:index_next_column]
            col_info = list(dict.fromkeys(col_info))
            doc_info[relevant_span_details_texts[0]] = col_info
            if index_next_column != 0:
                relevant_span_details_texts = relevant_span_details_texts[index_next_column:]
            else:
                relevant_span_details_texts = []
            # print(index_next_column, relevant_span_texts)

    dictNewRow = {}
    for c in output_columns:
        dictNewRow[c] = None

    #order preserving unique items, list(set([...]) won't do ths
    brief = []
    for r in relevant_span_top_texts:
        if r not in brief:
            brief.append(r)


    dictNewRow['ID'] = id
    dictNewRow['Brief'] = [brief]
    for c in output_columns:
        if c in doc_info:
            dictNewRow[c] = [doc_info[c]]
    dfNewRow = pd.DataFrame.from_dict(dictNewRow, orient='columns')
    
    df = pd.concat([df, dfNewRow])
    print(df)

    df.to_csv(FileLocations.fileLocationArticlesData, index=False)
    
    index_entry += 1
    print(f"Recording {index_entry} unique articles over {(time.time() - time_started) / 60:0.2f} minutes.")

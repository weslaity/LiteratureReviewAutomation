from bs4 import BeautifulSoup
from bs4.diagnose import diagnose

from selenium import webdriver  
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time

class Browser:

    def __init__(self):
        # not working? version error? update chrome, dl matching update https://googlechromelabs.github.io/chrome-for-testing/#stable
        self.driver = webdriver.Chrome()

    def GoTo(self, url):
        self.url = url
        self.driver.get(self.url)

    def GetCookies(self):
        return self.driver.get_cookies()

    def Retry(self):
        temp = self.url
        self.GoTo("http://www.blankwebsite.com/")
        time.sleep(1)
        self.GoTo(temp)

    def WaitFor(self, elementId, search, timeout):
        time_between_attempts = 5
        index_href = -1
        while (timeout > 0 and index_href == -1):
            attempts = 5
            loaded = False
            while attempts > 0 and not loaded:
                try:
                    element = WebDriverWait(self.driver, 20).until(
                        EC.presence_of_element_located((By.ID, elementId))
                    )
                    loaded = True
                except:
                    self.Retry()
                    attempts -= 1 
            if attempts == 0:
                return None
            strFoundSoup = element.get_attribute('innerHTML')

            try:
                index_href = strFoundSoup.index(search)
                return strFoundSoup
            except:
                index_href = -1

                timeout -= time_between_attempts
                time.sleep(time_between_attempts)
        print(f"Error: Could not find '{search}' from site {self.url}")
        return None

    def Close(self):
        self.driver.quit()


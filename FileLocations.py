fileLocationQueriesInput = "search_queries_input.txt"
fileLocationQueriesVisited = "search_queries_visited.txt"
fileLocationArticlesOutput = "found_articles.txt"

fileLocationArticlesBlacklist = "articles_blacklist.csv"
fileLocationArticlesData = "articles_data.csv"
fileLocationArticlesClean = "articles_clean.csv"
fileLocationArticlesRefined = "articles_refined.csv"
import time
import pandas as pd
import numpy as np
import re

import FileLocations

def strip_quotes(q_string):
    return q_string[1:-1]


def get_type(df):
    pieces = list(map(strip_quotes, df.strip()[1:-1].split(",")))
    return pieces[0]


def get_year(df):
    numbers = list(map(int, re.findall(r'\d+', df)))
    numbers = [x for x in numbers if x < 2024]
    numbers.sort()
    if len(numbers) == 0:
        return 0
    return numbers[-1]


df_articles = pd.read_csv(FileLocations.fileLocationArticlesData)
df_articles['Type'] = df_articles['Brief'].apply(get_type)
df_articles['YearParsed'] = df_articles['Brief'].apply(get_year)

print(df_articles['Type'])
print(df_articles['YearParsed'])

df_articles.to_csv(FileLocations.fileLocationArticlesClean, index=False)